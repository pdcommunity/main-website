---
title: بنیاد موزیلا
employee: 80
industry: software
type: nonprofit
revenue: 436
marketCap: na
links:
  website: https://www.mozilla.org/
  wikipedia: https://en.wikipedia.org/wiki/Mozilla_Corporation
---

همچنین
[شرکت موزیلا](../mozilla-corporation/)
را ببینید.
