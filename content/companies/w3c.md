---
title: کنسرسیوم وب جهانی
employee: 60
industry: software
type: nonprofit
business: seatSell
marketCap: na
revenue: 10
links:
  website: https://www.w3.org/
  wikipedia: https://en.wikipedia.org/wiki/World_Wide_Web_Consortium
---

کنسرسیوم وب جهانی یک سازمان غیر انتفاعی است که استاندارد های وب را تعیین می کند. این
سازمان توسط بنیان گذار وب جهانی، تیم برنر لی به وجود آمد تا از آن حمایت کند. وب
یکی از بستر هایی است که از آغاز تشکیلش، بر پایه داده های عمومی عمل می کرد و
تمام قرارداد ها و استاندارد های این بنیاد، بدون نیاز به پرداخت حق پتنت یا حق کپی رایت
یا ... برای عموم قابل استفاده و تغییر است. هر کسی می تواند یک مرورگر وب تولید کند
یا یک پایگاه وب راه اندازی کند. استاندارد های دیگر این سازمان، در جاهایی خارج از فضای
وب نیز استفاده می شوند. مثلا فرمت عکس برداری
svg
یک فرمت شناخته شده برای عکس است که استفاده و پیاده سازی از آن برای عموم آزاد است.
(بر خلاف برخی فرمت های مشابه)
یا جاوا اسکریپت که یک استاندارد تعیین شده توسط این بنیاد است اما تبدیل به یک
زبان برنامه نویسی همه منظوره شده است.

## مدل درآمد زایی
این سازمان هزینه های خود را از این سه طریق تامین می کند:
* فروش صندلی (حق عضویت) که خریداران آن می توانند در تصمیمات این کنسرسیوم مشارکت کنند
و حق رای داشته باشند.
* انجام پروژه های تحقیقاتی برای دولت ها و سازمان های دیگر
* کمک های مردمی و قبول اسپانسر

